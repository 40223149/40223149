
import os
import cherrypy

# 這個自訂函式呼叫時以輸入變數控制後續的列印內容
def 列印星號(輸入變數, n, d):
    outstring = "1"
    # 數列1 為[0, 1, ... 輸入變數-1]=>[0,1,2,3,4]
    數列1 = list(range(輸入變數))
    # 數列2 為數列1 各元素加上輸入變數-1, 目的在移動位置=>[4,5,6,7,8]
    數列2 = [x+輸入變數-1 for x in 數列1]
    # 反數列2 為數列1 元素反置, 也就是[輸入變數-1, ..., 0]=>[4,3,2,1,0]
    反數列2 = reversed(數列1)
    # 利用 zip() 將 數列2 與反數列2 組成並列元組, 目的在同一行列印時能夠同時列出兩數列所宣告的位置=>[4,5,6,7,8][4,3,2,1,0],[[4,4],[5,3],[6,2],[7,1],[8,0]]
    集合 = zip(數列2, 反數列2)
    # 利用迴圈逐行列印, 並以元組索引的元素判斷列印位置
    g = 0
    for t in range(n):
        for 索引 in 集合:
            for 數 in range(輸入變數*2-1):
                if 數 == 索引[0] or 數 == 索引[1]:
                    outstring += "＠"
                else:
                    outstring += "　"
            outstring += "<br />"
        數列1 = list(range(輸入變數))
        數列2 = [x+輸入變數-1 for x in 數列1]
        反數列2 = reversed(數列1)
        集合 = zip(數列2, 反數列2)
        for y in range(輸入變數*2-1):
            outstring += "＠"
        outstring +="<br />"
        for p in range(d) :
            for 數 in range(輸入變數*2-1):
                g += 1
                if g == 輸入變數-1 :
                    outstring += "＠"
                else:
                    if g == 輸入變數+1 :
                        outstring += "＠"
                    if g != 輸入變數+1:
                        outstring += "　"
            g = 0
            outstring += "<br />"
        outstring += "<br />"

    return outstring

class tree(object):
    @cherrypy.expose
    def index(self, num=5, n=5, d=3):
        outstring = ""
        outstring += 列印星號(int(num), int(n), int(d))
        return outstring+ "<br /><br /><a href='/'>回首頁</a>"

if __name__ == '__main__':
    # 假如在 os 環境變數中存在 'OPENSHIFT_REPO_DIR', 表示程式在 OpenShift 環境中執行
    if 'OPENSHIFT_REPO_DIR' in os.environ.keys():
        # 雲端執行啟動
        application = cherrypy.Application(tree())
    else:
        # 近端執行啟動
        '''
        cherrypy.server.socket_port = 8083
        cherrypy.server.socket_host = '127.0.0.1'
        '''
        cherrypy.quickstart(tree())
