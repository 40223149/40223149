#coding: utf-8
'''
2013 年  Fall C1 程式語言 期末專案 v1
在雲端執行時, 必須在 persistent data 目錄下新增 downloads, tmp 目錄
'''
import cherrypy
import os
import math
from cherrypy.lib.static import serve_file

# 確定程式檔案所在目錄, 在 Windows 有最後的反斜線
_curdir = os.path.join(os.getcwd(), os.path.dirname(__file__))

# 設定在雲端與近端的資料儲存目錄
if 'OPENSHIFT_REPO_DIR' in os.environ.keys():
    # 表示程式在雲端執行
    download_root_dir = os.environ['OPENSHIFT_DATA_DIR']
    data_dir = os.environ['OPENSHIFT_DATA_DIR']
else:
    # 表示程式在近端執行
    download_root_dir = _curdir + "local_data/"
    data_dir = _curdir + "local_data/"

def downloadlist_access_list(files, starti, endi):
    # 可以透過不同檔案提供不同的連結檢視
    # 影像檔案, 影音檔案或零件 STL 檔案以跳出視窗檢視, 其餘則直接下載
    # files 為全部要列出的所有資料,  從 starti 列到 endi
    outstring = ""
    for 索引 in range(int(starti)-1, int(endi)):
        fileName, fileExtension = os.path.splitext(files[索引])
        if fileExtension == ".png" or fileExtension == ".jpg" or fileExtension == ".gif":
            outstring += '<a href="javascript:;" onClick="window.open(\'/downloads/'+ \
            files[索引]+'\',\'images\', \'catalogmode\',\'scrollbars\')">'+files[索引]+'</a><br />'
        else:
            outstring += "<a href='/download/?filepath="+download_root_dir.replace('\\', '/')+ \
            "downloads/"+files[索引]+"'>"+files[索引]+"</a><br />"
    return outstring

class C12013Project(object):
    # 登入驗證後必須利用 session 機制儲存
    _cp_config = {
    # 配合 utf-8 格式之表單內容
    # 若沒有 utf-8 encoding 設定,則表單不可輸入中文
    'tools.encode.encoding': 'utf-8',
    # 加入 session 設定
    'tools.sessions.on' : True,
    'tools.sessions.storage_type' : 'file',
    'tools.sessions.locking' : 'explicit',
    # 就 OpenShift ./tmp 位於 app-root/runtime/repo/tmp
    # tmp 目錄與 wsgi 目錄同級
    'tools.sessions.storage_path' : data_dir+'tmp',
    # 內定的 session timeout 時間為 60 分鐘
    'tools.sessions.timeout' : 60
    }

    @cherrypy.expose
    def default(self, attr='default', *args, **kwargs):
        raise cherrypy.HTTPRedirect("/")

    @cherrypy.expose
    def index(self):
        return self.mymenu()

    @cherrypy.expose
    def inputdata(self):
        return "<form method='post' action='mysave'>stud no:<input type='text' name='studno' \
><br /><input type='submit' value='send'></form><br />"+self.mymenu()

    @cherrypy.expose
    def mysave(self, studno=None):
        檔案 = open(data_dir+"mydata_w.txt", "a", encoding="utf-8")
        檔案.write(studno+"\n")
        檔案.close()
        return "done"+self.mymenu()

    @cherrypy.expose
    def printdata(self):
        檔案 = open(data_dir+"mydata_w.txt", "r", encoding="utf-8")
        數列 = []
        for 行資料 in 檔案:
            數列.append(行資料.strip())
        檔案.close()
        outstring = ""
        for 索引 in range(len(數列)):
            outstring += str(索引)+":"+數列[索引]+"<br />"
        return outstring+"<br />"+self.mymenu()

    @cherrypy.expose
    def uploadform(self):
        return """
    <script src="/static/jquery.js" type="text/javascript"></script>
    <script src="/static/axuploader.js" type="text/javascript"></script>
    <script>
    $(document).ready(function(){
    $('.prova').axuploader({url:'/axupload', allowExt:['jpg','png','gif','7z','pdf'],
    finish:function(x,files)
            {
                alert('All files have been uploaded: '+files);
            },
    enable:true,
    remotePath:function(){
      return 'downloads/';
    }
    });
    });
    </script>
    <div class="prova"></div>
    <input type="button" onclick="$('.prova').axuploader('disable')" value="asd" />
    <input type="button" onclick="$('.prova').axuploader('enable')" value="ok" />
    """

    # 在近端與雲端上處理上傳檔案儲存目錄設定, 注意 downloads 前是否需要 /
    @cherrypy.expose
    def axupload(self, *args, **kwargs):
        filename = kwargs["ax-file-name"]
        flag = kwargs["start"]
        if flag == 0:
            file = open(download_root_dir+"downloads/"+filename, "wb")
        else:
            file = open(download_root_dir+"downloads/"+filename, "ab")
        file.write(cherrypy.request.body.read())
        file.close()
        return "已經完成檔案上傳"

    @cherrypy.expose
    def download_list(self, item_per_page=5, page=1, keyword=None):
        # cherrypy.session['admin'] = 1
        # cherrypy.session.get('admin')
        files = os.listdir(download_root_dir+"downloads/")
        total_rows = len(files)
        totalpage = math.ceil(total_rows/int(item_per_page))
        starti = int(item_per_page) * (int(page) - 1) + 1
        endi = starti + int(item_per_page) - 1
        outstring = ""
        notlast = False
        if total_rows > 0:
            # 準備在表格之前列印頁數資料
            # 開始最前頭的頁數資料列印
            outstring += "<br />"
            if (int(page) * int(item_per_page)) < total_rows:
                notlast = True
            if int(page) > 1:
                # 列出前往第一頁的連結
                outstring += "<a href='"
                outstring += "/download_list?&amp;page=1&amp;item_per_page="+str(item_per_page)+"&amp;keyword="+str(cherrypy.session.get('download_keyword'))
                outstring += "'><<</a> "
                page_num = int(page) - 1
                outstring += "<a href='"
                outstring += "/download_list?&amp;page="+str(page_num)+"&amp;item_per_page="+str(item_per_page)+"&amp;keyword="+str(cherrypy.session.get('download_keyword'))
                outstring += "'>上一頁</a> "
            # 這裡希望能夠將總頁數以每 20 頁進行分段顯示,最多顯示出 span * 2 的頁數
            span = 10
            for 索引 in range(int(page)-span, int(page)+span):
            #for ($j=$page-$range;$j<$page+$range;$j++)
                if 索引>= 0 and 索引< totalpage:
                    page_now = 索引 + 1 
                    if page_now == int(page):
                        outstring += "<font size='+1' color='red'>"+str(page)+" </font>"
                    else:
                        outstring += "<a href='"
                        outstring += "/download_list?&amp;page="+str(page_now)+"&amp;item_per_page="+str(item_per_page)+"&amp;keyword="+str(cherrypy.session.get('download_keyword'))
                        outstring += "'>"+str(page_now)+"</a> "

            if notlast == True:
                nextpage = int(page) + 1
                outstring += " <a href='"
                outstring += "/download_list?&amp;page="+str(nextpage)+"&amp;item_per_page="+str(item_per_page)+"&amp;keyword="+str(cherrypy.session.get('download_keyword'))
                outstring += "'>下一頁</a>"
                #列出前往最後一頁的連結
                outstring += " <a href='"
                outstring += "/download_list?&amp;page="+str(totalpage)+"&amp;item_per_page="+str(item_per_page)+"&amp;keyword="+str(cherrypy.session.get('download_keyword'))
                outstring += "'>>></a><br /><br />"
            # 完成最前頭的頁數資料
            # 列印最外圍的內容
            if (int(page) * int(item_per_page)) < total_rows:
                notlast = True
                outstring += downloadlist_access_list(files, starti, endi)+"<br />"
            else:
                outstring += "<br /><br />"
                outstring += downloadlist_access_list(files, starti, total_rows)+"<br />"
            
            if int(page) > 1:
                #列出前往第一頁的連結
                outstring += "<a href='"
                outstring += "/download_list?&amp;page=1&amp;item_per_page="+str(item_per_page)+"&amp;keyword="+str(cherrypy.session.get('download_keyword'))
                outstring += "'><<</a> "
                page_num = int(page) - 1
                outstring += "<a href='"
                outstring += "/download_list?&amp;page="+str(page_num)+"&amp;item_per_page="+str(item_per_page)+"&amp;keyword="+str(cherrypy.session.get('download_keyword'))
                outstring += "'>上一頁</a> "
            # 這裡希望能夠將總頁數以每 20 頁進行分段顯示,最多顯示出 span * 2 的頁數
            span = 10
            for 索引 in range(int(page)-span, int(page)+span):
            #for ($j=$page-$range;$j<$page+$range;$j++)
                if 索引 >=0 and 索引 < totalpage:
                    page_now = 索引 + 1
                    if page_now == int(page):
                        outstring += "<font size='+1' color='red'>"+str(page)+" </font>"
                    else:
                        outstring += "<a href='"
                        outstring += "/download_list?&amp;page="+str(page_now)+"&amp;item_per_page="+str(item_per_page)+"&amp;keyword="+str(cherrypy.session.get('download_keyword'))
                        outstring += "'>"+str(page_now)+"</a> "
            if notlast == True:
                nextpage = int(page) + 1
                outstring += " <a href='"
                outstring += "/download_list?&amp;page="+str(nextpage)+"&amp;item_per_page="+str(item_per_page)+"&amp;keyword="+str(cherrypy.session.get('download_keyword'))
                outstring += "'>下一頁</a>"
                # 列出前往最後一頁的連結
                outstring += " <a href='"
                outstring += "/download_list?&amp;page="+str(totalpage)+"&amp;item_per_page="+str(item_per_page)+"&amp;keyword="+str(cherrypy.session.get('download_keyword'))
                outstring += "'>>></a>"
        else:
            outstring += "沒有資料!"

        return outstring+"<br/><br />"+self.mymenu()

    # 就學生分組資料中的 data[6]+data[7] 學號最後兩碼數字轉為整數作為排序依據
    #學生分組資料 = sorted(學生分組資料, key=lambda data: int(data[6]+data[7]))

    @cherrypy.expose
    def mymenu(self):
        outstring = '''<a href='/'>首頁</a><br /> \
<a href='/inputdata'>輸入基本資料</a><br /> 
<a href='/printdata'>列印基本資料</a><br /> 
<a href='/uploadform'>上傳檔案表單</a><br /> 
<a href='/download_list'>上傳檔案列表</a><br /> 
'''
        return outstring

class Download:
    @cherrypy.expose
    def index(self, filepath):
        return serve_file(filepath, "application/x-download", "attachment")

# 配合程式檔案所在目錄設定靜態目錄或靜態檔案
application_conf = {'/static':{
        'tools.staticdir.on': True,
        'tools.staticdir.dir': _curdir+"/static"},
        '/downloads':{
        'tools.staticdir.on': True,
        'tools.staticdir.dir': data_dir+"/downloads"}
    }

root = C12013Project()
root.download = Download()

# 假如在 os 環境變數中存在 'OPENSHIFT_REPO_DIR', 表示程式在 OpenShift 環境中執行
if 'OPENSHIFT_REPO_DIR' in os.environ.keys():
    # 雲端執行啟動
    application = cherrypy.Application(root, config = application_conf)
else:
    # 近端執行啟動
    '''
    cherrypy.server.socket_port = 8083
    cherrypy.server.socket_host = '127.0.0.1'
    '''
    cherrypy.quickstart(root, config = application_conf)