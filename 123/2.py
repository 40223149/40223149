<!DOCTYPE html>  
<html>  
  <head>  
     <title>HTML 5 DEMO</title> 
     <style>
       header, nav, section, article, footer {
         display: block;
       }
       header, nav, section, article, footer {
         color: white;
       }
       header, footer {
         text-align: center;
         width: 100%;
       }
       header {
         background-color: red;
         font-size: 36px;
         font-weight: bold;
       }
       nav {
         position: fixed;
         top: 40px;
         right: 25px;
         background-color: blue;
         width: 100px;
       }
       section {
         width: 86%;
         background-color: gray;
         padding: 20px;
         margin: 20px;
       }
       footer {
         background-color: green;
         font-size: 10px;
       }
       table { 
         border-collapse: collapse; 
         border: solid thick; 
       }
       colgroup, tbody { 
         border: solid medium; 
       }
       td { 
         border: solid thin; 
         height: 1.4em; 
         width: 1.4em; 
         text-align: center; 
         padding: 0; 
       }
     </style> 
  </head>  
  <body>  
    <header>  
       header  
    </header>  
    <nav>  
      <ul>
        <li>nav 1</li>
        <li>nav 2</li>
      </ul>  
    </nav>
    <section>
      <article>
        <table>
          <caption>Today's Sudoku</caption>
          <colgroup><col><col><col>
          <colgroup><col><col><col>
          <colgroup><col><col><col>
          <thead>
            <tr> <th colspan="9"> begin
          </thead>
          <tbody>
            <tr> <td> 1 <td>   <td> 3 <td> 6 <td>   <td> 4 <td> 7 <td>   <td> 9
            <tr> <td>   <td> 2 <td>   <td>   <td> 9 <td>   <td>   <td> 1 <td>
            <tr> <td> 7 <td>   <td>   <td>   <td>   <td>   <td>   <td>   <td> 6
          </tbody>
          <tbody>
            <tr> <td> 2 <td>   <td> 4 <td>   <td> 3 <td>   <td> 9 <td>   <td> 8
            <tr> <td>   <td>   <td>   <td>   <td>   <td>   <td>   <td>   <td>
            <tr> <td> 5 <td>   <td>   <td> 9 <td>   <td> 7 <td>   <td>   <td> 1
          </tbody>
          <tbody>
            <tr> <td> 6 <td>   <td>   <td>   <td> 5 <td>   <td>   <td>   <td> 2
            <tr> <td>   <td>   <td>   <td>   <td> 7 <td>   <td>   <td>   <td>
            <tr> <td> 9 <td>   <td>   <td> 8 <td>   <td> 2 <td>   <td>   <td> 5
          </tbody>
          <tfoot>
            <tr> <td colspan="9"> the end
          </tfoot>
        </table>
      </article>
    </section>      
    <footer>  
      <p>
        footer © 2011 
      </p>       
    </footer>  
  </body>  
</html>  